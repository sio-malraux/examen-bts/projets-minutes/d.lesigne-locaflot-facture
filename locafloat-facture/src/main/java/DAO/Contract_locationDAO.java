package DAO;

import classes.Contract_location;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author nonovan
 *
 */
public class Contract_locationDAO extends DAO<Contract_location> {
	
	// Insertion d'un objet contrat dans la table contrat (1 ligne)
	@Override
	public void create(Contract_location obj) {
		try {
			 	PreparedStatement prepare = this.connect
	                                        .prepareStatement("INSERT INTO \"locafot-facture\".contrat_location VALUES(?, ?, ?, ?)"
	                                         );
			 	prepare.setInt(1, obj.getId());
				prepare.setString(2, obj.getDate());
				prepare.setString(3, obj.getHeure_debut());
				prepare.setString(4, obj.getHeure_fin());
				prepare.executeUpdate();  
			}
		catch (SQLException e) {
		e.printStackTrace();
		} 
	}
	
	// Recherche d'un contrat par rapport à son id	
	@Override
	public Contract_location read(String code) {
		int id;
		id = Integer.parseInt(code);
		
		Contract_location leContrat = new Contract_location();
		try {
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"locafot-facture\".contrat_location WHERE id = '" + id +"'");
	          
	          if(result.first())
	        	  leContrat = new Contract_location(id, result.getString("date"),result.getString("heure_debut"),result.getString("heure_fin"));   
	    }
		catch (SQLException e) {
			        e.printStackTrace();
		}
		return leContrat;	
	}
	
	// Mise à jour d'un contrat
	@Override
	public void update(Contract_location obj) {
		try {
				PreparedStatement prepare = this.connect
                .prepareStatement("UPDATE \"locaflot_facture\".contrat_location SET date = ?, heure_debut = ?, heure_fin = ? WHERE id = ?");
			  
				prepare.setString(1, obj.getDate());
				prepare.setString(2, obj.getHeure_debut());
				prepare.setString(3, obj.getHeure_fin());
				prepare.setInt(4, obj.getId());
				prepare.executeUpdate();
				
				obj = this.read(Integer.toString(obj.getId()));
		}
		catch (SQLException e) {
		      e.printStackTrace();
		}
	}

	// Suppression d'un contrat
	@Override
	public void delete(Contract_location obj) {
		try {
			
			this.connect
            .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
           		            ResultSet.CONCUR_UPDATABLE)
            .executeUpdate("DELETE FROM \"locaflot_facture\".louer WHERE id_contrat = " + obj.getId() + ";");

	           this.connect
	               .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	               .executeUpdate("DELETE FROM \"locafot-facture\".contrat_location WHERE id = '" + obj.getId()+"'");
		 }
		catch (SQLException e) {
		            e.printStackTrace();
		}
	}

	@Override
	public List<Contract_location> recupAll() {
		List<Contract_location> listeContrat = new ArrayList<Contract_location>(); 
		
		try {
			Statement requete = this.connect.createStatement();

			ResultSet curseur = requete.executeQuery("select * from \"locaflot-facture\".contrat_location");

			while (curseur.next()){
				Contract_location unContrat = new Contract_location();
				
				unContrat.setId(curseur.getInt("id"));
				unContrat.setDate(curseur.getString("date"));
				unContrat.setHeure_debut(curseur.getString("heure_debut"));
				unContrat.setHeure_fin(curseur.getString("heure_fin"));
				listeContrat.add(unContrat);
			}

			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		return listeContrat; 
	}
}
