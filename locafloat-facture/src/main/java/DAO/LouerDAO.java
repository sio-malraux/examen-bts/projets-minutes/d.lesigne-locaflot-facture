package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import classes.Contract_location;
import classes.Embarcation;
import classes.Louer;

public class LouerDAO extends DAO<Louer> {

	// Insertion d'un objet louer dans la table louer (1 ligne)
		@Override
		public void create(Louer obj) {
			try {
				 	PreparedStatement prepare = this.connect
		                                        .prepareStatement("INSERT INTO \"locafot-facture\".louer VALUES(?, ?, ?)"
		                                         );
				 	prepare.setInt(1, obj.getId_contract().getId());
					prepare.setString(2, obj.getId_embarcation().getId());
					prepare.setInt(3, obj.getNb_personnes());
					prepare.executeUpdate();  
				}
			catch (SQLException e) {
			e.printStackTrace();
			} 
		}
		
		@Override
		public Louer read(String code)
		{
			return null;
		}

		
		// Recherche d'un loueur par rapport à ses id	
		
		public Louer read(int contract_id, String embarcation_id) {
			
			Louer leloueur = new Louer();
			try {
		          ResultSet result = this.connect
		                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
		                                                ResultSet.CONCUR_UPDATABLE)
		                             .executeQuery("SELECT * FROM \"locafot-facture\".louer WHERE id_contrat  = '" + contract_id +" AND id_embarcation = '"+ embarcation_id + "'");
		          
		          if(result.first()) {
		        	  EmbarcationDAO embarcation_DAO = new EmbarcationDAO();
		        	  Contract_locationDAO contract_DAO = new Contract_locationDAO();
		        	  leloueur = new Louer(contract_DAO.read(Integer.toString(contract_id)), embarcation_DAO.read(embarcation_id),result.getInt("nb_personnes"));   
		          }
		    }
			catch (SQLException e) {
				        e.printStackTrace();
			}
			return leloueur;	
		}
		
		// Mise à jour d'une embarcation
		@Override
		public void update(Louer obj) {
			try { PreparedStatement prepare = this.connect.prepareStatement
					("UPDATE \"locafot-facture\".embarcation SET nb_personnes = ? WHERE id_contrat = ? AND id_embarcation = ?" );
							  
			prepare.setInt(1, obj.getNb_personnes());
			prepare.setInt(2, obj.getId_contract().getId());
			prepare.setString(3, obj.getId_embarcation().getId());
			prepare.executeUpdate();

			}
			catch (SQLException e) {
			      e.printStackTrace();
			}
		}
		
		// Suppression d'une embarcation
		@Override
		public void delete(Louer obj) {
			try {
		           this.connect
		               .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
		            		            ResultSet.CONCUR_UPDATABLE)
		               .executeUpdate("DELETE FROM \"locafot-facture\".louer WHERE id_contrat  = '" + obj.getId_contract().getId() + " AND id_embarcation = '" + obj.getId_embarcation().getId() + "'");
			 }
			catch (SQLException e) {
			            e.printStackTrace();
			}
		}
		
		@Override
		public List<Louer> recupAll() {
			List<Louer> liste_loueur = new ArrayList<Louer>(); 
			try {
				Statement requete = this.connect.createStatement();
				ResultSet curseur = requete.executeQuery("select * from \"locaflot-facture\".louer");
				while (curseur.next()){
					Louer un_loueur = new Louer();
					EmbarcationDAO embarcation_DAO = new EmbarcationDAO();
					Contract_locationDAO contract_DAO = new Contract_locationDAO();
					
					un_loueur.setId_contract(contract_DAO.read(Integer.toString(curseur.getInt("id_contrat"))));
					un_loueur.setId_embarcation(embarcation_DAO.read(curseur.getString("id_embarcation")));
					un_loueur.setNb_personnes(curseur.getInt("nb_personnes"));
					liste_loueur.add(un_loueur);
				}

				curseur.close();
				requete.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} 
			
			return liste_loueur; 
		}
}
