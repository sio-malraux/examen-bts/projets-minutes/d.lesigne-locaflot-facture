package DAO;
import java.sql.Connection;

import java.util.List;

/**
 * 
 * @author nonovan
 *
 * @param <T>
 */
public abstract class DAO<T> {

	public Connection connect = ConnexionPostgreSql.getInstance();
	
	/**
	 * 
	 * @param obj
	 */
	public abstract void create(T obj);
	
	/**
	 * 
	 * @param code
	 * @return
	 */
	public abstract T read (String code);
	
	/**
	 * 
	 * @param obj
	 */
	public abstract void update(T obj);
	
	/**
	 * 
	 * @param obj
	 */
	public abstract void delete(T obj);
	
	/**
	 *
	 * @return A list of objects.
	 */

	public abstract List<T> recupAll();
	
}
