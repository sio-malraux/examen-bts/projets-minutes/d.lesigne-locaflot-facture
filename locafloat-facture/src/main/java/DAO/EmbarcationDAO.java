package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import classes.Embarcation;

public class EmbarcationDAO extends DAO<Embarcation>{
	
	// Insertion d'un objet embarcation dans la table embarcation
	@Override
	public void create(Embarcation obj) {
		try {
			 	PreparedStatement prepare = this.connect
	                                        .prepareStatement("INSERT INTO \"locafot-facture\".embarcation VALUES(?, ?, ?, ?)"
	                                         );
			 	prepare.setString(1, obj.getId());
				prepare.setString(2, obj.getCouleur());
				prepare.setBoolean(3, obj.getDisponible());
				prepare.setString(4, obj.getType().getCode());
					
				prepare.executeUpdate();
			}
		catch (SQLException e) {
		e.printStackTrace();
		} 
	}
	
	// Recherche d'une embarcation par rapport à son id	
	@Override
	public Embarcation read(String code) {
		Embarcation lembarcation = new Embarcation();
		try {
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"locafot-facture\".embarcation WHERE id = '" + code +"'");
	          
	          if(result.first()) {
	        	  Type_embarcationDAO type_DAO = new Type_embarcationDAO();
	        	  lembarcation = new Embarcation(code, result.getString("couleur"),result.getBoolean("disponible"),type_DAO.read(result.getString("type")));   
	    }}
		catch (SQLException e) {
			        e.printStackTrace();
		}
		return lembarcation;
	}
	
	// Mise à jour d'une embarcation
	@Override
	public void update(Embarcation obj) {
		try { PreparedStatement prepare = this.connect.prepareStatement
				("UPDATE \"locafot-facture\".embarcation SET couleur = ?, disponible = ?, type = ?" + " WHERE id = ?" );
						  
			  prepare.setString(1, obj.getCouleur());
			  prepare.setBoolean(2, obj.getDisponible());
			  prepare.setString(3, obj.getType().getCode());
			  prepare.setString(4, obj.getId());
			  prepare.executeUpdate();
		}
		catch (SQLException e) {
		      e.printStackTrace();
		}
	}

	// Suppression d'une embarcation
	@Override
	public void delete(Embarcation obj) {
		try {
			this.connect
            .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
           		            ResultSet.CONCUR_UPDATABLE)
            .executeUpdate("DELETE FROM \"locaflot_facture\".louer WHERE id_embarcation = '" + obj.getId() + "'");

	           this.connect
	               .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	               .executeUpdate("DELETE FROM \"locafot-facture\".embarcation WHERE id = '" + obj.getId()+"'");
		 }
		catch (SQLException e) {
		            e.printStackTrace();
		}
	}
	
	@Override
	public List<Embarcation> recupAll() {
		List<Embarcation> listeEmbarcation = new ArrayList<Embarcation>(); 
		try {
			Statement requete = this.connect.createStatement();
			ResultSet curseur = requete.executeQuery("select * from \"locaflot-facture\".embarcation");
			while (curseur.next()){
				Embarcation uneEmbarcation = new Embarcation();
				Type_embarcationDAO type_DAO = new Type_embarcationDAO();
				
				uneEmbarcation.setId(curseur.getString("id"));
				uneEmbarcation.setCouleur(curseur.getString("couleur"));
				uneEmbarcation.setDisponible(curseur.getBoolean("disponible"));
				uneEmbarcation.setType(type_DAO.read(curseur.getString("type")));
				listeEmbarcation.add(uneEmbarcation);
			}

			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		return listeEmbarcation; 
	}
}