package DAO;

import classes.Type_embarcation;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Type_embarcationDAO extends DAO<Type_embarcation>{
	
	// Insertion d'un Type_embarcation dans la table Type_embarcation (1 ligne)
	@Override
	public void create(Type_embarcation obj) {
		try {
			 	PreparedStatement prepare = this.connect
	                                        .prepareStatement("INSERT INTO \"locafot-facture\".type_embarcation VALUES(?, ?, ?, ?, ?, ?, ?)"
	                                         );
			 	prepare.setString(1, obj.getCode());
				prepare.setString(2, obj.getNom());
				prepare.setInt(3, obj.getNb_place());
				prepare.setDouble(4, obj.getPrix_demi_heure());
				prepare.setDouble(5, obj.getPrix_heure());
				prepare.setDouble(6, obj.getPrix_demi_jour());
				prepare.setDouble(7, obj.getPrix_jour());
					
				prepare.executeUpdate();
			}
		catch (SQLException e) {
		e.printStackTrace();
		} 
	}

	// Recherche d'un Type_embarcation par rapport à son code	
	@Override
	public Type_embarcation read(String code) {
	
		Type_embarcation leType = new Type_embarcation();
		try {
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"locafot-facture\".type_embarcation WHERE code = '" + code +"'");
	          
	          if(result.first())
	        	  leType = new Type_embarcation(code, result.getString("nom"),result.getInt("nb_place"),result.getDouble("Prix_demi_heure"),result.getDouble("Prix_heure"),result.getDouble("Prix_demi_jour"),result.getDouble("Prix_jour"));   
	    }
		catch (SQLException e) {
			        e.printStackTrace();
		}
		return leType;
	}
	
	// Mise à jour d'un Type_embarcation
	@Override
	public void update(Type_embarcation obj) {
		try { PreparedStatement prepare = this.connect
				.prepareStatement("UPDATE \"locaflot_facture\".type_embarcation "
	               		+ "SET nom = ?, nb_place = ?, prix_demi_heure = ?, prix_heure = ?, prix_demi_jour = ?, prix_jour = ? WHERE code = ?" );
		prepare.setString(1, obj.getNom());
		prepare.setInt(2, obj.getNb_place());
		prepare.setDouble(3, obj.getPrix_demi_heure());
		prepare.setDouble(4, obj.getPrix_heure());
		prepare.setDouble(5, obj.getPrix_demi_jour());
		prepare.setDouble(6, obj.getPrix_jour());
		prepare.setString(7, obj.getCode());

		  prepare.executeUpdate();
			  obj = this.read(obj.getCode());
		}
		catch (SQLException e) {
		      e.printStackTrace();
		}
	}

	// Suppression d'un Type_embarcation
	@Override
	public void delete(Type_embarcation obj) {
		try {
			this.connect
            .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
           		            ResultSet.CONCUR_UPDATABLE)
            .executeUpdate("DELETE FROM \"locaflot_facture\".louer WHERE id_embarcation IN (SELECT id FROM \"locaflot_facture\".embarcation WHERE type = '" + obj.getCode()+"')");
			
			this.connect
            .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
           		            ResultSet.CONCUR_UPDATABLE)
            .executeUpdate("DELETE FROM \"locaflot_facture\".embarcation WHERE type = '" + obj.getCode()+"'");
			
	         this.connect
	             .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	             .executeUpdate("DELETE FROM \"locaflot_facture\".type_embarcation WHERE code = '" + obj.getCode()+"'");

		 }
		catch (SQLException e) {
		            e.printStackTrace();
		}
	}
	
	@Override
	public List<Type_embarcation> recupAll() {
		List<Type_embarcation> listeType = new ArrayList<Type_embarcation>(); 
		
		try {
			Statement requete = this.connect.createStatement();

			ResultSet curseur = requete.executeQuery("select * from \"locaflot-facture\".type_embarcation");

			while (curseur.next()){
				Type_embarcation unType = new Type_embarcation();
				
				unType.setCode(curseur.getString("code"));
				unType.setNom(curseur.getString("nom"));
				unType.setNb_place(curseur.getInt("nb_place"));
				unType.setPrix_demi_heure(curseur.getDouble("Prix_demi_heure"));
				unType.setPrix_heure(curseur.getDouble("Prix_heure"));
				unType.setPrix_demi_jour(curseur.getDouble("Prix_demi_jour"));
				unType.setPrix_jour(curseur.getDouble("Prix_jour"));		
				listeType.add(unType);
			}

			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		return listeType; 
	}
}
