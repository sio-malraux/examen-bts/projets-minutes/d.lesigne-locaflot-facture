package classes;

import classes.Embarcation;
/**
 * 
 * @author nonovan
 *
 */
public class Embarcation {

	private String id;
	private String couleur;
	private Boolean disponible;
	private Type_embarcation type;

	/**
	 * 
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @return couleur
	 */
	public String getCouleur() {
		return couleur;
	}
	
	/**
	 * 
	 * @param couleur
	 */
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	
	/**
	 * 
	 * @return disponible
	 */
	public Boolean getDisponible() {
		return disponible;
	}
	
	/**
	 * 
	 * @param disponible
	 */
	public void setDisponible(Boolean disponible) {
		this.disponible = disponible;
	}
	
	/**
	 * 
	 * @return type
	 */
	public Type_embarcation getType() {
		return type;
	}
	
	/**
	 * 
	 * @param type
	 */
	public void setType(Type_embarcation type) {
		this.type = type;
	}
	
	/**
	 * Constructeur par defaut
	 */
	public Embarcation() {
		id = null;
		couleur = null;
		disponible = null;
		type = new Type_embarcation();
	}
	
	/**
	 * Constructeur avec parametre
	 * @param id
	 * @param couleur
	 * @param disponible
	 * @param type
	 */
	public Embarcation(String id, String couleur, Boolean disponible, Type_embarcation type) {
		this.id = id;
		this.couleur = couleur;
		this.disponible = disponible;
		this.type = type;
	}
	
	/**
	 * @return id,couleur,disponible,type
	 */
	@Override
	public String toString() {
		return "\n\nEmbarcation : \nId : " + id + "\nCouleur : " + couleur + "\nDisponible : " + disponible + "\nType : "
				+ type;
	}
}
