package classes;

import classes.Contract_location;
import classes.Embarcation;

/**
 * 
 * @author nonovan
 *
 */
public class Louer {
	
	private Contract_location id_contract;
	private Embarcation id_embarcation;
	private Integer nb_personnes;
	
	/**
	 * 
	 * @return id_contract
	 */
	public Contract_location getId_contract() {
		return id_contract;
	}

	/**
	 * 
	 * @param id_contract
	 */
	public void setId_contract(Contract_location id_contract) {
		this.id_contract = id_contract;
	}
	
	/**
	 * 
	 * @return id_embarcation
	 */
	public Embarcation getId_embarcation() {
		return id_embarcation;
	}

	/**
	 * 
	 * @param id_embarcation
	 */
	public void setId_embarcation(Embarcation id_embarcation) {
		this.id_embarcation = id_embarcation;
	}
	
	/**
	 * 
	 * @return nb_personnes
	 */
	public Integer getNb_personnes() {
		return nb_personnes;
	}

	/**
	 * 
	 * @param nb_personnes
	 */
	public void setNb_personnes(Integer nb_personnes) {
		this.nb_personnes = nb_personnes;
	}
	
	/**
	 * Constructeur par defaut
	 */
	public Louer() {
		id_contract = new Contract_location();
		id_embarcation = new Embarcation();
		nb_personnes = 0;
	}
	
	/**
	 * Constructeur avec parametre
	 * @param id_contract
	 * @param id_embarcation
	 * @param nb_personnes
	 */
	public Louer(Contract_location id_contract, Embarcation id_embarcation, Integer nb_personnes) {
		this.id_contract = id_contract;
		this.id_embarcation = id_embarcation;
		this.nb_personnes = nb_personnes;
	}
	
	/**
	 * @return id_contract,id_embarcation,nb_personnes
	 */
	@Override
	public String toString() {
		return "\n\nEmbarcation : " +
				"\nId_contract : " + id_contract + 
				"\nId_embarcation : " + id_embarcation +
				"\nNb_personnes : " + nb_personnes;
				
	}
}
