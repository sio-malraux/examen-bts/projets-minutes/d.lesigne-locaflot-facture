package classes;

/**
 * 
 * @author nonovan
 *
 */
public class Type_embarcation {
	private String code;
	private String nom;
	private Integer nb_place;
	private Double prix_demi_heure;
	private Double prix_heure;
	private Double prix_demi_jour;
	private Double prix_jour;
	
	/**
	 * 
	 * @return code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * 
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * 
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * 
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * 
	 * @return nb_place
	 */
	public Integer getNb_place() {
		return nb_place;
	}
	
	/**
	 * 
	 * @param nb_place
	 */
	public void setNb_place(Integer nb_place) {
		this.nb_place = nb_place;
	}
	
	/**
	 * 
	 * @return prix_demi_heure
	 */
	public Double getPrix_demi_heure() {
		return prix_demi_heure;
	}
	
	/**
	 * 
	 * @param prix_demi_heure
	 */
	public void setPrix_demi_heure(Double prix_demi_heure) {
		this.prix_demi_heure = prix_demi_heure;
	}
	
	/**
	 * 
	 * @return prix_heure
	 */
	public Double getPrix_heure() {
		return prix_heure;
	}
	
	/**
	 * 
	 * @param prix_heure
	 */
	public void setPrix_heure(Double prix_heure) {
		this.prix_heure = prix_heure;
	}
	
	/**
	 * 
	 * @return prix_demi_jour
	 */
	public Double getPrix_demi_jour() {
		return prix_demi_jour;
	}
	
	/**
	 * 
	 * @param prix_demi_jour
	 */
	public void setPrix_demi_jour(Double prix_demi_jour) {
		this.prix_demi_jour = prix_demi_jour;
	}
	
	/**
	 * 
	 * @return prix_jour
	 */
	public Double getPrix_jour() {
		return prix_jour;
	}
	
	/**
	 * 
	 * @param prix_jour
	 */
	public void setPrix_jour(Double prix_jour) {
		this.prix_jour = prix_jour;
	}
	
	/**
	 * Constructeur par defaut
	 */
	public Type_embarcation() {
		code = null;
		nom = null;
		nb_place = 0;
		prix_demi_heure = 0.0;
		prix_heure = 0.0;
		prix_demi_jour = 0.0;
		prix_jour = 0.0;
	}
	
	/**
	 * Constructeur avec parametre
	 * @param code
	 * @param nom
	 * @param nb_place
	 * @param prix_demi_heure
	 * @param prix_heure
	 * @param prix_demi_jour
	 * @param prix_jour
	 */
	public Type_embarcation(String code, String nom, Integer nb_place, Double prix_demi_heure, Double prix_heure, Double prix_demi_jour, Double prix_jour) {
		this.code = code;
		this.nom = nom;
		this.nb_place = nb_place;
		this.prix_demi_heure = prix_demi_heure;
		this.prix_heure = prix_heure;
		this.prix_demi_jour = prix_demi_jour;
		this.prix_jour = prix_jour;
	}
	
	/**
	 * @return code,nom,nb_place,prix_demi_heure,prix_heure,prix_demi_jour,prix_jour
	 */
	@Override
	public String toString() {
		return "\n\nTypeEmbarcation : "+ 
				"\nCode : " + code + 
				"\nNom : " + nom + 
				"\nNb_place : " + nb_place + 
				"\nPrix_demi_heure : " + prix_demi_heure + 
				"\nPrix_heure : " + prix_heure + 
				"\nPrix_demi_jour : " + prix_demi_jour + 
				"\nPrix_jour : " + prix_jour; 
	}
}
