package classes;

/**
 * 
 * @author nonovan
 *
 */
public class Contract_location {
	private Integer id;
	private String date;
	private String heure_debut;
	private String heure_fin;

	/**
	 * 
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return date
	 */
	public String getDate() {
		return date;
	}
	
	/**
	 * 
	 * @param date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * 
	 * @return heure_debut
	 */
	public String getHeure_debut() {
		return heure_debut;
	}
	
	/**
	 * 
	 * @param heure_debut
	 */
	public void setHeure_debut(String heure_debut) {
		this.heure_debut = heure_debut;
	}
	
	/**
	 * 
	 * @return heure_fin
	 */
	public String getHeure_fin() {
		return heure_fin;
	}
	
	/**
	 * 
	 * @param heure_fin
	 */
	public void setHeure_fin(String heure_fin) {
		this.heure_fin = heure_fin;
	}
	
	/**
	 * Constructeur par defaut
	 */
	public Contract_location() {
		id = 0;
		date = null;
		heure_debut = null;
		heure_fin = null;
	}
	
	/**
	 * Constructeur avec parametre
	 * @param id
	 * @param date
	 * @param heure_debut
	 * @param heure_fin
	 */
	public Contract_location(Integer id, String date, String heure_debut, String heure_fin) {
		this.id = id;
		this.date = date;
		this.heure_debut = heure_debut;
		this.heure_fin = heure_fin;
	}
	
	/**
	 * @return id,date,heure_debut,heure_fin
	 */
	@Override
	public String toString() {
		return "\n\nContrat location : \nId : " + id + "\nDate : " + date + "\nHeure_debut : " + heure_debut + "\nHeure_fin : "
				+ heure_fin;
	}
}